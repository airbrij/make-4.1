FROM ubuntu:20.04

RUN \
  apt update -y && \
  apt install -y wget build-essential

WORKDIR /root

RUN \
  wget http://archive.ubuntu.com/ubuntu/pool/main/m/make-dfsg/make-dfsg_4.1.orig.tar.gz && \
  wget http://archive.ubuntu.com/ubuntu/pool/main/m/make-dfsg/make-dfsg_4.1-9.1ubuntu1.diff.gz

RUN \
  DEBIAN_FRONTEND=noninteractive apt install -y gettext po-debconf debhelper dh-autoreconf autoconf automake autopoint file pkg-config guile-2.0-dev libbsd-resource-perl

RUN \
  tar -zxvf make-dfsg_4.1.orig.tar.gz && \
  gzip -d make-dfsg_4.1-9.1ubuntu1.diff.gz && \
  cd make-dfsg-4.1 && \
  patch -p1 <../make-dfsg_4.1-9.1ubuntu1.diff 

RUN \
  cd make-dfsg-4.1 && \
  DEB_BUILD_OPTIONS="nodocs nocheck" dpkg-buildpackage -rfakeroot -b
